//
//  CollectioViewController.m
//  collectionViewDemo2
//
//  Created by Ankur on 24/11/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import "CollectioViewController.h"
#import "MyCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
@interface CollectioViewController ()<UIScrollViewDelegate>{

    NSMutableArray *arrayImages;
    int imageNoToShare;

}

@end

@implementation CollectioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageCache = [[NSCache alloc] init];

    arrayImages=[[NSMutableArray alloc]init];
    [arrayImages addObject:@"http://www.androidapk.cc/wp-content/uploads/2012/04/3D_Speed_Racing_Car_1.jpg"];
    [arrayImages addObject:@"http://www.phtheme.com/androidimg/allimg/120712/2-120G21625120-L.jpg"];
    [arrayImages addObject:@"http://www.whitegadget.com/attachments/wallpaper/46850d1294877933-car-wallpapers-33852-sports_car_3.sports-car-3.jpg"];
    [arrayImages addObject:@"http://www.build-iphone.com/uploads/soft/iPhone%20Wallpaper/Car_/Red%20car%20iphone%20wallpaper.jpg"];
    [arrayImages addObject:@"http://www.leawo.com/free-resources/lib/upload/ipod-iphone-wallpapers/Car/race-car.jpg"];
    [arrayImages addObject:@"http://www.phtheme.com/androidimg/allimg/120629/2-1206291A2350-L.jpg"];
    [arrayImages addObject:@"http://down2.9apps.com:7080/group1/M00/00/D0/oYYBAFQSeIOAQaatAABrc8U5oiA920.jpg"];
    [arrayImages addObject:@"http://www.iphoneheat.com/wp-content/uploads/2009/05/iphone-wallpapers-cars-53.jpg"];
    [arrayImages addObject:@"http://www.leawo.com/free-resources/lib/upload/ipod-iphone-wallpapers/Car/nature-car.jpg"];
    [arrayImages addObject:@"http://www.iphoneheat.com/wp-content/uploads/2009/05/iphone-wallpapers-cars-05.jpg"];
    
    [self.collectionView registerClass:[MyCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
   
    //[self.collectionView setSectionInset:UIEdgeInsetsMake(collectionViewHeight / 2, 0, collectionViewHeight / 2, 0)];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayImages count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    BHAlbumPhotoCell *photoCell =
    //    [collectionView dequeueReusableCellWithReuseIdentifier:PhotoCellIdentifier
    //                                              forIndexPath:indexPath];
    //
    //    return photoCell;
    
    MyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    //    cell.imageVu.image = [UIImage imageNamed:@"image.png"];
    
    NSString *imagePath=[arrayImages objectAtIndex:indexPath.row];

    NSString *stringName=[imagePath lastPathComponent];
    UIImage *cachedImage =   [self.imageCache objectForKey:stringName];

    if (cachedImage)
    {
        cell.imageVu.image = cachedImage;
        NSLog(@"Already Cached imaged");
    }
    else{
       // [cell.imageVu setImageWithURL:[NSURL URLWithString:[arrayImages objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@""]];
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]];
            UIImage *image    = nil;
            if (imageData)
                image = [UIImage imageWithData:imageData];
            
            [self.imageCache setObject:image forKey:stringName];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageVu.image=image;

                NSLog(@"image downloaded successfully");
            });
        });
    
    }
    
    
    
    
    UITapGestureRecognizer *tapTwice =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTwice:)];
    tapTwice.numberOfTapsRequired = 2;
    [cell.scrollView_ addGestureRecognizer:tapTwice];
    cell.scrollView_.delegate = self;
    
    
    UIPinchGestureRecognizer *twoFingerPinch = [[UIPinchGestureRecognizer alloc]
                      initWithTarget:self
                      action:@selector(twoFingerPinch:)];
    [cell.imageVu addGestureRecognizer:twoFingerPinch];
    
    return cell;
}

- (void)twoFingerPinch:(UIPinchGestureRecognizer *)recognizer
{
    UIImageView *scrollerImageVU=(UIImageView *)recognizer.view;

    //    NSLog(@"Pinch scale: %f", recognizer.scale);
    if (recognizer.scale >1.0f && recognizer.scale < 2.0f) {
        CGAffineTransform transform = CGAffineTransformMakeScale(recognizer.scale, recognizer.scale);
        scrollerImageVU.transform = transform;
    }
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"indexpath %i",indexPath.row);
 //   [self.collectionView scrollToItemAtIndexPath:indexPath
                                //atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                  //      animated:YES];
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    for (MyCollectionViewCell *cell in [self.collectionView visibleCells]) {
        [cell.scrollView_ setZoomScale:cell.scrollView_.minimumZoomScale animated:YES];
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        NSLog(@"visibleCells %ld",(long)indexPath.row);
        imageNoToShare=(int)indexPath.row;
    }
    
}


- (void)tapTwice:(UIGestureRecognizer *)gestureRecognizer {
    
    UIScrollView *scroller=(UIScrollView *)gestureRecognizer.view;
    NSLog(@"cell.scrollView_Maximum %f cell.scrollView_Minimum %f cell.scrollView_Current %f",scroller.maximumZoomScale,scroller.minimumZoomScale,scroller.zoomScale);
           if(scroller.zoomScale > scroller.minimumZoomScale)
            [ scroller setZoomScale:scroller.minimumZoomScale animated:YES];
            else
            [ scroller  setZoomScale:scroller.maximumZoomScale animated:YES];
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return scrollView.subviews.firstObject;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    
} 



- (IBAction)shareBtnClicked:(id)sender {
    NSLog(@"imagenotoshare %d \n imagePath %@",imageNoToShare,[arrayImages objectAtIndex:imageNoToShare]);
    

    
}

- (IBAction)closeBtnClicked:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)moveToSpecific:(id)sender {
    
    //[UIView animateWithDuration:0 animations:^{
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    //}];

    NSString *imagePath=[arrayImages objectAtIndex:imageNoToShare];
    NSString *stringName=[imagePath lastPathComponent];

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]];
        UIImage *image    = nil;
        if (imageData)
            image = [UIImage imageWithData:imageData];
        
       [self.imageCache setObject:image forKey:stringName];
               dispatch_async(dispatch_get_main_queue(), ^{
                   NSLog(@"image downloaded successfully");
        });
    });
    
    
}
@end
