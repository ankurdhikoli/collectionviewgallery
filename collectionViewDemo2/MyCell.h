//
//  MyCell.h
//  collectionViewDemo2
//
//  Created by Ankur Chauhan on 11/23/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageVu;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView_;

@end
