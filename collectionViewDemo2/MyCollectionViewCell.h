//
//  MyCollectionViewCell.h
//  collectionViewDemo2
//
//  Created by Ankur on 24/11/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView_;
@property (strong, nonatomic) IBOutlet UIImageView *imageVu;

@end
