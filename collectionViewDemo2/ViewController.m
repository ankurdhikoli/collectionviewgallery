//
//  ViewController.m
//  collectionViewDemo2
//
//  Created by Ankur Chauhan on 11/23/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import "ViewController.h"
#import "MyCell.h"
#import "UIImageView+AFNetworking.h"
//#import "MyCollectionViewCell.h"
#import "CollectioViewController.h"

@interface ViewController ()<UIScrollViewDelegate>
{

    NSMutableArray *arrayImages;

}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrayImages=[[NSMutableArray alloc]init];
    [arrayImages addObject:@"http://www.androidapk.cc/wp-content/uploads/2012/04/3D_Speed_Racing_Car_1.jpg"];
    [arrayImages addObject:@"http://www.phtheme.com/androidimg/allimg/120712/2-120G21625120-L.jpg"];
    [arrayImages addObject:@"http://www.whitegadget.com/attachments/wallpaper/46850d1294877933-car-wallpapers-33852-sports_car_3.sports-car-3.jpg"];
    [arrayImages addObject:@"http://www.build-iphone.com/uploads/soft/iPhone%20Wallpaper/Car_/Red%20car%20iphone%20wallpaper.jpg"];
    [arrayImages addObject:@"http://www.leawo.com/free-resources/lib/upload/ipod-iphone-wallpapers/Car/race-car.jpg"];
    [arrayImages addObject:@"http://www.phtheme.com/androidimg/allimg/120629/2-1206291A2350-L.jpg"];
    [arrayImages addObject:@"http://down2.9apps.com:7080/group1/M00/00/D0/oYYBAFQSeIOAQaatAABrc8U5oiA920.jpg"];
    [arrayImages addObject:@"http://www.iphoneheat.com/wp-content/uploads/2009/05/iphone-wallpapers-cars-53.jpg"];
    [arrayImages addObject:@"http://www.leawo.com/free-resources/lib/upload/ipod-iphone-wallpapers/Car/nature-car.jpg"];
    [arrayImages addObject:@"http://www.iphoneheat.com/wp-content/uploads/2009/05/iphone-wallpapers-cars-05.jpg"];

  // [self.collectionView registerClass:[MyCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
//{
//    return 2;
//}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrayImages count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    BHAlbumPhotoCell *photoCell =
//    [collectionView dequeueReusableCellWithReuseIdentifier:PhotoCellIdentifier
//                                              forIndexPath:indexPath];
//    
//    return photoCell;
    
    MyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
//    cell.imageVu.image = [UIImage imageNamed:@"image.png"];
    [cell.imageVu setImageWithURL:[NSURL URLWithString:[arrayImages objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@""]];
    
    
    UITapGestureRecognizer *tapTwice =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapTwice:)];
    
    tapTwice.numberOfTapsRequired = 2;
    [cell.scrollView_ addGestureRecognizer:tapTwice];
    cell.scrollView_.delegate = self;

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"indexpath %i",indexPath.row);
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    for (UICollectionViewCell *cell in [self.collectionView visibleCells]) {
        //[cell.scrollView_ setZoomScale:cell.scrollView_.minimumZoomScale animated:YES];
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        NSLog(@"visibleCells %ld",(long)indexPath.row);
    }
}

- (void)tapTwice:(UIGestureRecognizer *)gestureRecognizer {
    
    UIScrollView *scroller=(UIScrollView *)gestureRecognizer.view;
    NSLog(@"cell.scrollView_Maximum %f cell.scrollView_Minimum %f cell.scrollView_Current %f",scroller.maximumZoomScale,scroller.minimumZoomScale,scroller.zoomScale);
    if(scroller.zoomScale > scroller.minimumZoomScale)
        [ scroller setZoomScale:scroller.minimumZoomScale animated:YES];
    else
        [ scroller  setZoomScale:scroller.maximumZoomScale animated:YES];
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return scrollView.subviews.firstObject;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    
}


- (IBAction)xibButtonClicked:(id)sender {
    
    CollectioViewController *collectobj =  [[CollectioViewController alloc] initWithNibName:@"CollectioViewController" bundle:nil];
   [self.navigationController presentViewController:collectobj animated:YES completion:nil];
    
}
@end
