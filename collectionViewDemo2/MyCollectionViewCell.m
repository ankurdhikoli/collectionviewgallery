//
//  MyCollectionViewCell.m
//  collectionViewDemo2
//
//  Created by Ankur on 24/11/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import "MyCollectionViewCell.h"

@implementation MyCollectionViewCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSArray *arrayOfviews = [[NSBundle mainBundle]loadNibNamed:@"MyCollectionViewCell" owner:self options:nil];
        self = [[arrayOfviews objectAtIndex:0]isKindOfClass:[UICollectionViewCell class]] ? [arrayOfviews objectAtIndex:0] : nil;
        // Initialization code
    }
    return self;
}
@end
