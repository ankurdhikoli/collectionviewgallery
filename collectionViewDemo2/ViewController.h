//
//  ViewController.h
//  collectionViewDemo2
//
//  Created by Ankur Chauhan on 11/23/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)xibButtonClicked:(id)sender;

@end
