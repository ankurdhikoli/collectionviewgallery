//
//  CollectioViewController.h
//  collectionViewDemo2
//
//  Created by Ankur on 24/11/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectioViewController : UIViewController{
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSCache *imageCache;

- (IBAction)shareBtnClicked:(id)sender;
- (IBAction)closeBtnClicked:(id)sender;
- (IBAction)moveToSpecific:(id)sender;

@end
